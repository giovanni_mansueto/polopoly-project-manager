# Polopoly Project Manager

Gerencia as configurações para os projetos Polopoly.

Desenvolvido com: AngularJS, Sqlite, Electron e Bootstrap.
<br>
Exemplo para tutorial no [Clube dos Geeks](http://clubedosgeeks.com.br/programacao/node-js/criando-aplicativo-com-electron-gerando-executavel)
## Usando
#### 1 - Instale as dependências
```
npm install
```
#### 2 - Execute o aplicativo
```
npm start
```
#### 3 - Gerando Executável<br>
[Veja aqui](http://clubedosgeeks.com.br/programacao/node-js/criando-aplicativo-com-electron-gerando-executavel#executar) como criar o executável.
