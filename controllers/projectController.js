'use strict';

app.controller('projectController', function($scope, $location, dbService) {
	// debug only
	window.dbService = dbService;

	$scope.mainPath = '';
	$scope.currentProject = {};


	dbService.runAsync('SELECT * FROM mainPaths', function(data) {
		if(data && data.length){
			$scope.mainPath = data[0].path;
		}
	});

	//Listando
	$scope.listProjects = function() {
		dbService.runAsync('SELECT * FROM projects WHERE active = 1', function(data) {
			data = (data || []);
			$scope.currentProject = data.find((e) => e.isCurrent);
			$scope.projects = data;
		});
	}

	$scope.setAsCurrent = function(project) {
		ipcRenderer.send('app-set-current-project', {
			project,
			mainPath: $scope.mainPath
		});

		$scope.projects
				.filter((e) => e.id !== project.id)
				.map((e) => {
					e.isCurrent = 0;
					$scope.save(e);
				});

		project.isCurrent = 1;
		$scope.currentProject = project;

		$scope.save(project);
	}

	//Salvando
	$scope.save = function(project) {
		if (project.id) {
			//Editar
			var id = project.id;
			delete project.id;
			delete project.$$hashKey; //Apaga elemento $$hashKey do objeto
			dbService.update('projects', project, {
				id: id
			}); //entidade, data, where
		} else {
			//nova
			project.active = 1;
			project.isCurrent = 0;
			dbService.insert('projects', project); // entidade, data
		}
		$scope.project = {};
		$scope.listProjects();
		$('#modalPessoa').modal('hide');
	}

	//Abrindo para edit
	$scope.edit = function(data) {
		$scope.project = data;
		$('#modalPessoa').modal('show');
	}

	//Excluindo
	$scope.delete = function(data) {
		if (confirm('Deseja realmente apagar o cadastro de ' + data.name + '?')) {
			dbService.update('projects', {
				active: 0
			}, {
				id: data.id
			});
			$scope.listProjects();
		}
	}


	$scope.$watch('mainPath', function(newVal) {
		dbService.update('mainPaths', {
			path: newVal
		}, {
			id: 1
		});
	});
});