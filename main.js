const electron = require('electron');
const ipcMain = electron.ipcMain;
const fs = require('fs');
const path = require('path');
var app = require('app');
var BrowserWindow = require('browser-window');

// referência global para manter a instância da janela até que sejam fechadas pelo usuário então ele irá ser fechado quando o JavaScript fizer Garbage collection
var mainWindow = null;

// Sair da aplicação quando todas as janelas forem fechadas
app.on('window-all-closed', function() {
	if (process.platform != 'darwin') {
		app.quit();
	}
});

app.on('ready', function() {
	// Cria a janela do browser.
	mainWindow = new BrowserWindow({
		width: 700,
		height: 600
	});

	// Carrega o arquivo html principal.
	mainWindow.loadURL('file://' + __dirname + '/index.html');

	// aber o DevTools. (console, inspecionar elemento, etc)
	// mainWindow.webContents.openDevTools();

	// Evento emitido quando a janela é fechada, usado para destruir instancia.
	mainWindow.on('closed', function() {
		mainWindow = null;
	});
});

ipcMain.on('app-set-current-project', (event, arg) => {
	fs.readFile(path.join(__dirname, 'base', 'settings.xml'), 'utf-8', (err, data) => {
		if (err) {
			console.log(err.message);
			return;
		}

		console.log('on settings.xml data', data);

		updateSettingsFile(data, arg);
	});
});

function updateSettingsFile(fileString, args) {
	function replaceAll(target, search, replacement) {
	    return target.split(search).join(replacement);
	};

	var settingsPath = path.join(args.mainPath, 'settings.xml');
	var fileData = replaceAll(fileString, '[___DBNAME___]', args.project.dbName);

	fs.writeFile(settingsPath, fileData, (err) => {
	    if (err) {
	        console.log(err.message);
	        return;
	    }

	    console.log('Saved');
	});
}