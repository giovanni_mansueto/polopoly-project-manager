'use strict';
const path = require('path');

app.factory('dbService', function($http){
	var sqlite = require('sqlite-sync');
	var db = sqlite.connect(path.join(__dirname, 'model', 'database.db')); //database.db
	return db;
});