var app = angular.module('xprog', [require('angular-route'),'angularUtils.directives.dirPagination']);

app.config(function($routeProvider){
	$routeProvider.when('/', {
		templateUrl : 'views/project.html',
		controller : 'projectController',
        access: { requiredLogin: false }
	});
});